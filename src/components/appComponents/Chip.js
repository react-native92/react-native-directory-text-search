import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import Labels from '../../utils/Strings';
import Styles from '../../styles/otherStyles/Chip';
import * as HelperStyles from '../../utils/HelperStyles';

const Chip = ({
  containerStyle = null,
  label = Labels.button,
  mode = Labels.outlined,
  onPress = () => {},
  textStyle = null,
  touchable = true,
}) => {
  // Other Variables
  let customContainerStyle = {},
    customTextStyle = {};

  // Theme Variables
  const Theme = useTheme().colors;

  switch (mode) {
    case Labels.outlined:
      customContainerStyle = {
        ...customContainerStyle,
        backgroundColor: Colors.transparent,
        borderColor: Theme.text,
      };

      customTextStyle = {color: Theme.text};
      break;

    case Labels.solid:
    default:
      customContainerStyle = {
        ...customContainerStyle,
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
      };

      customTextStyle = {color: Colors.white};
      break;
  }

  const handleOnPress = () => {
    onPress();
  };

  const renderChip = () => {
    return (
      <Text
        style={[
          HelperStyles.textView(
            12,
            '600',
            Colors.white,
            'center',
            'capitalize',
          ),
          customTextStyle,
          textStyle,
        ]}>
        {label}
      </Text>
    );
  };

  return (
    <TouchableOpacity
      disabled={touchable}
      style={[Styles.buttonContainer, customContainerStyle, containerStyle]}
      onPress={() => {
        handleOnPress();
      }}>
      {renderChip()}
    </TouchableOpacity>
  );
};

export default Chip;
