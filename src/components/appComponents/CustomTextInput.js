import React from 'react';
import {TextInput, TouchableOpacity, View} from 'react-native';
import Colors from '../../utils/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import Labels from '../../utils/Strings';
import Styles from '../../styles/otherStyles/CustomTextInput';
import * as HelperStyles from '../../utils/HelperStyles';

const CustomTextInput = ({
  autoCorrect = true,
  autoFocus = false,
  containerStyle = {},
  iconContainerStyle = {},
  iconSize = 16,
  iconStyle = {},
  isSearch = false,
  keyboardType = 'default',
  onChangeText = () => {},
  onClose = () => {},
  placeholder = Labels.text,
  placeholderTextColor = Colors.slateGray,
  textInputContainerStyle = {},
  textInputStyle = {},
  value = null,
}) => {
  // CustomTextInput Variables

  return (
    <View style={[Styles.container, containerStyle]}>
      {Boolean(isSearch) && (
        <View
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
            iconContainerStyle,
          ]}>
          <FontAwesomeIcons
            color={Colors.slateGray}
            name={'search'}
            size={iconSize}
            style={[HelperStyles.padding(2, 2), iconStyle]}
          />
        </View>
      )}

      <View
        style={[
          HelperStyles.flex(Boolean(isSearch) && Boolean(value) ? 0.8 : 0.9),
          HelperStyles.justView('justifyContent', 'center'),
          textInputContainerStyle,
        ]}>
        <TextInput
          autoCorrect={autoCorrect}
          autoFocus={autoFocus}
          keyboardType={keyboardType}
          onChangeText={txt => {
            onChangeText(txt);
          }}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          style={[
            HelperStyles.justView('height', 40),
            HelperStyles.padding(Boolean(isSearch) ? 4 : 8, 0),
            HelperStyles.textView(14, '600', Colors.slateGray, 'left', 'none'),
            textInputStyle,
          ]}
          value={value}
        />
      </View>

      {Boolean(value) && (
        <TouchableOpacity
          onPress={() => {
            onClose();
          }}
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
            iconContainerStyle,
          ]}>
          <FontAwesomeIcons
            color={Colors.slateGray}
            name={'times'}
            size={iconSize}
            style={[HelperStyles.padding(2, 2), iconStyle]}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CustomTextInput;
