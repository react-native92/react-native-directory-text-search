import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Labels from '../utils/Strings';
import Dashboard from '../screens/appScreens/Dashboard';

const AppNavigation = () => {
  // AppNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group>
      <Stack.Screen
        name={Labels.dashboard}
        component={Dashboard}
        options={{
          gestureEnabled: false,
          headerBackVisible: false,
        }}
      />
    </Stack.Group>
  );
};

export default AppNavigation;
