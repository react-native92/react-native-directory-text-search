const strings = {
  // App Strings
  dark: 'dark',

  // AppNavigation Strings
  dashboard: 'Dashboard',

  // AuthNavigation Strings
  splash: 'Splash',

  // Button Strings
  button: 'Button',
  light: 'light',
  solid: 'solid',

  // Chip Strings
  outlined: 'outlined',

  // CustomModal Strings
  defaultModalMessage: 'Please! Enter any message!',
  hint: 'Hint',
  confirm: 'Confirm',
  cancel: 'Cancel',

  // Dashboard Strings
  exitApp: 'Are you sure that you want to exit RN DirText Search App?',
  selectDirCache: 'Sorry! Unable to select the directory!',
  files: 'File(s)',
  documents: 'Documents',
  downloads: 'Downloads',
  create: 'Create',
  delete: 'Delete',
  sampleFileCache: 'Sorry! Unable to create / delete / update the file(s)!',
  search: 'Search',
  counts: 'Counts',
  replaceModal: 'Replace Modal',
  replace: 'Replace',
  success: 'Success',
  replaceSuccess: 'The word has been replaced successfully in the dir file(s)!',
  openPathCache: 'Sorry! Unable to open the path!',

  // NoResponse Strings
  whoops: 'Whoops!',
  notFindAnything: "We couldn't find anything!",

  // Other Strings
  error: 'Error',

  // TextInput Strings
  text: 'Text',

  // Security Strings
  background: 'background',
  inactive: 'inactive',
};

export default strings;
