import {Dimensions, useColorScheme} from 'react-native';
import Labels from './Strings';

// Common Data
export const dirctories = [Labels.downloads, Labels.documents];

export const unSupportedExtensions = [
  'jpg',
  'jpeg',
  'mp3',
  'mp4',
  'pdf',
  'png',
  'svg',
  'zip',
];

// Field Validation Functions
export const checkField = str => {
  return str && str.trim();
};

// Get Screen Height
export const screenHeight = Dimensions.get('screen').height;

// Get Screen Weight
export const screenWidth = Dimensions.get('screen').width;

// Get Window Height
export const windowHeight = Dimensions.get('window').height;

// Get Window Weight
export const windowWidth = Dimensions.get('window').width;

// Regex Functions
export const validateEmail = emailId => {
  const emailRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return !emailRegex.test(String(emailId).toLowerCase());
};

export const validatePassword = password => {
  const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,20}$/;

  return !passwordRegex.test(String(password));
};

// Theme Functions
export const getThemeScheme = () => {
  return useColorScheme();
};
