const colors = {
  // Theme Hex Colors
  primary: '#DE1C50',
  secondary: '#215E90',
  tertiary: '#5D8A49',

  // App Hex Colors
  black: '#000000',
  darkCharcoal: '#333333',
  ghostWhite: '#F7F7FA',
  lightGrey: '#D3D3D3',
  lightText: '#98A5C4',
  martinique: '#3A3246',
  red: '#FF0000',
  slateGray: '#707C97',
  solitude: '#EEF2F8',
  transparent: 'transparent',
  white: '#FFFFFF',

  // App RGBA Colors
  white125: 'rgba(255, 255, 255, 0.125)',
  black125: 'rgba(0, 0, 0, 0.125)',
  stroke50: 'rgba(229, 232, 239, 0.5)',
};

export default colors;
