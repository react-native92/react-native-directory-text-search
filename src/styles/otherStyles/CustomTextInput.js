import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const CustomTextInput = StyleSheet.create({
  container: {
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'space-between',
    borderColor: Colors.slateGray,
    borderWidth: 2,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export default CustomTextInput;
