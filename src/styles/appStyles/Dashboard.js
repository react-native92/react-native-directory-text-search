import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const Dashboard = StyleSheet.create({
  directoryContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 8,
    paddingVertical: 2,
  },
  chipSelected: {
    backgroundColor: Colors.tertiary,
    borderColor: Colors.tertiary,
  },
  fileContainer: {
    width: '47.5%',
    overflow: 'hidden',
    flexDirection: 'column',
    marginVertical: 8,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  replaceButtonContainer: {
    width: '75%',
    alignSelf: 'center',
    marginVertical: 16,
  },
  skeletonCardContainer: {
    height: Helpers.windowHeight * 0.14375,
    width: Helpers.windowWidth * 0.455,
    borderRadius: 8,
  },
});

export default Dashboard;
