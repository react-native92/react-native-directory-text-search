import React, {useCallback, useState} from 'react';
import {
  BackHandler,
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import {zip} from 'react-native-zip-archive';
import Button from '../../components/appComponents/Button';
import Card from '../../containers/Card';
import Chip from '../../components/appComponents/Chip';
import Colors from '../../utils/Colors';
import CustomModal from '../../components/appComponents/CustomModal';
import CustomTextInput from '../../components/appComponents/CustomTextInput';
import FakeData from '../../utils/FakeData';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import Labels from '../../utils/Strings';
import NoResponse from '../../components/appComponents/NoResponse';
import RNFetchBlob from 'rn-fetch-blob';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Styles from '../../styles/appStyles/Dashboard';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Dashboard = props => {
  // Dashboard Variables
  const [selectedDirs, setSelectedDirs] = useState([Labels.downloads]);
  const [files, setFiles] = useState(null);
  const [searchQuery, setSearchQuery] = useState(null);
  const [totalWordCounts, setTotalWordCounts] = useState(null);
  const [replaceQuery, setReplaceQuery] = useState(null);

  // Other Variables
  const [exitApp, setExitApp] = useState(false);
  const [loading, setLoading] = useState(true);
  const [replaceModal, setReplaceModal] = useState(false);
  const [convertStatus, setConvertStatus] = useState(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () => {
        isFocus = false;

        BackHandler.removeEventListener('hardwareBackPress', backAction);
      };
    }, [exitApp]),
  );

  const backAction = function () {
    setExitApp(true);

    return true;
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      createFilestoDir(Labels.downloads, FakeData);

      return () => {
        isFocus = false;

        setConvertStatus(false);
      };
    }, []),
  );

  const createFilestoDir = (dirName, fileContent) => {
    switch (dirName) {
      case Labels.documents:
        sampleFiles(Labels.create, getDirPaths(dirName), fileContent);
        break;

      case Labels.downloads:
        sampleFiles(Labels.create, getDirPaths(dirName), fileContent);
        break;

      default:
        break;
    }
  };

  const deleteFilestoDir = dirName => {
    switch (dirName) {
      case Labels.documents:
        sampleFiles(Labels.delete, getDirPaths(dirName));
        break;

      case Labels.downloads:
        sampleFiles(Labels.delete, getDirPaths(dirName));
        break;

      default:
        break;
    }
  };

  const getDirPaths = dirName => {
    let dirPath = null;

    switch (dirName) {
      case Labels.documents:
        dirPath = RNFetchBlob.fs.dirs.DocumentDir;
        break;

      case Labels.downloads:
        dirPath = RNFetchBlob.fs.dirs.DownloadDir;
        break;

      default:
        break;
    }

    return dirPath;
  };

  const sampleFiles = (action, dirPath, fileContent) => {
    sampleFile(action, dirPath + '/Sample - I.txt', fileContent);
    sampleFile(action, dirPath + '/Sample - II.txt', fileContent);
    sampleFile(action, dirPath + '/Sample - III.txt', fileContent);
  };

  const sampleFile = async (action, filePath, fileContent) => {
    try {
      switch (action) {
        case Labels.create:
          RNFetchBlob.fs.writeStream(filePath, 'utf8').then(stream => {
            stream.write(fileContent);

            return stream.close();
          });
          break;

        case Labels.delete:
          await RNFetchBlob.fs.unlink(filePath);
          break;

        default:
          break;
      }
    } catch (error) {
      showMessage({
        description: Labels.sampleFileCache,
        icon: 'auto',
        message: Labels.error,
        type: 'danger',
      });
    }
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      setLoading(true);

      setTimeout(() => {
        fetchDirFiles();

        setLoading(false);
      }, 2500);

      return () => {
        isFocus = false;
      };
    }, [selectedDirs]),
  );

  const fetchDirFiles = async () => {
    let helperArray = [];

    if (Array.isArray(selectedDirs) && selectedDirs.length != 0) {
      selectedDirs.forEach(dirName => {
        let dir = getDirPaths(dirName);

        if (Boolean(dir)) {
          RNFetchBlob.fs
            .lstat(dir)
            .then(result => {
              helperArray = [...helperArray, ...result];

              helperArray = filterFilesExtension(helperArray);

              setFiles(helperArray);

              setLoading(false);
            })
            .catch(err => {
              showMessage({
                description: `Sorry! Unable to pull the file(s) from ${dirName} dir!`,
                icon: 'auto',
                message: Labels.error,
                type: 'danger',
              });
            });
        } else {
          showMessage({
            description: `Sorry! Unable to pull the file(s) from ${dirName} dir!`,
            icon: 'auto',
            message: Labels.error,
            type: 'danger',
          });
        }
      });
    } else {
      setFiles([]);
    }
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      searchWordInDirFiles();

      return () => {
        isFocus = false;
      };
    }, [searchQuery]),
  );

  const searchWordInDirFiles = async () => {
    if (Array.isArray(files) && files.length != 0) {
      if (Boolean(searchQuery)) {
        let wordCount = 0;

        await files.forEach((fileData, index) => {
          RNFetchBlob.fs.readStream(fileData.path, 'utf8').then(stream => {
            let content = '';

            stream.open();

            stream.onData(chunk => {
              content += chunk;
            });

            stream.onEnd(() => {
              const counts = content.split(searchQuery).length - 1;

              files[index].counts = counts;

              wordCount = wordCount + counts;

              setTotalWordCounts(String(wordCount));
            });
          });
        });
      } else {
        setTotalWordCounts(null);

        await files.forEach((_, index) => {
          files[index].counts = null;
        });
      }

      setFiles(files);
    } else {
      setFiles(null);
    }
  };

  const filterFilesExtension = filesArray => {
    return filesArray.filter(lol => {
      const fileExt = getFileExtension(lol.filename);

      return !Helpers.unSupportedExtensions.includes(fileExt);
    });
  };

  const getFileExtension = fileName => {
    return fileName.split('.')[1];
  };

  const handleDirFilesZipConvert = () => {
    selectedDirs.map(dirName => {
      handleZipConversion(dirName);
    });
  };

  const handleZipConversion = dirName => {
    const dirPath = getDirPaths(dirName);
    const targetPath = `${dirPath}/${dirName}.zip`;
    const sourcePath = dirPath;

    zip(sourcePath, targetPath)
      .then(path => {
        showMessage({
          description: `The file(s) under ${dirName} dir has been successfully!`,
          icon: 'auto',
          message: Labels.success,
          type: 'success',
        });

        setTimeout(() => {
          openPath(sourcePath);
        }, 2500);
      })
      .catch(error => {
        showMessage({
          description: `Sorry! Unable to zip the file(s) under ${dirName} dir!`,
          icon: 'auto',
          message: Labels.error,
          type: 'danger',
        });
      });
  };

  const openPath = (path = null) => {
    if (Boolean(path)) {
      RNFetchBlob.android.actionViewIntent(path, '*/*');
    } else {
      showMessage({
        description: Labels.openPathCache,
        icon: 'auto',
        message: Labels.error,
        type: 'danger',
      });
    }
  };

  const renderDirectory = () => {
    const renderItem = (lol, index) => {
      return (
        <Chip
          containerStyle={[
            HelperStyles.justView('marginLeft', index != 0 ? 8 : 0),
            selectedDirs.includes(lol) && Styles.chipSelected,
          ]}
          label={lol}
          mode={selectedDirs.includes(lol) ? Labels.solid : Labels.outlined}
          onPress={() => {
            handleChipSelection(lol);
          }}
          touchable={false}
        />
      );
    };

    return (
      <View style={Styles.directoryContainer}>
        <FlatList
          data={Helpers.dirctories}
          horizontal={true}
          keyboardShouldPersistTaps={'handled'}
          keyExtractor={(_, index) => index}
          renderItem={({item, index}) => renderItem(item, index)}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  };

  const handleChipSelection = dirName => {
    const helperArray = [...selectedDirs];

    setSearchQuery(null);

    setTotalWordCounts(null);

    setConvertStatus(false);

    setFiles(null);

    if (!helperArray.includes(dirName)) {
      helperArray.push(dirName);

      createFilestoDir(dirName, FakeData);
    } else {
      const index = helperArray.indexOf(dirName);

      index > -1 && [helperArray.splice(index, 1), deleteFilestoDir(dirName)];
    }

    setSelectedDirs(helperArray);
  };

  const renderSearchBar = () => {
    return (
      <CustomTextInput
        isSearch={true}
        onChangeText={txt => {
          setSearchQuery(txt);
        }}
        onClose={() => {
          setSearchQuery(null);
        }}
        placeholder={Labels.search}
        value={searchQuery}
      />
    );
  };

  const renderWordCounts = () => {
    return (
      <Text
        style={HelperStyles.textView(
          14,
          '600',
          Colors.secondary,
          'left',
          'none',
        )}>
        {Labels.counts}:{' '}
        <Text
          style={HelperStyles.textView(
            14,
            '800',
            Colors.lightText,
            'left',
            'none',
          )}>
          {totalWordCounts}
        </Text>
      </Text>
    );
  };

  const renderFiles = () => {
    const renderItem = (fileData, index) => {
      return (
        <Card
          key={index}
          containerStyle={[
            Styles.fileContainer,
            HelperStyles.justView(
              'marginRight',
              index % 2 == 0 || index == 0 ? 8 : 0,
            ),
          ]}
          disabled={false}
          onPress={() => {
            openPath(fileData?.path);
          }}>
          <View
            style={[
              HelperStyles.justView('backgroundColor', Colors.lightGrey),
              HelperStyles.justifyContentCenteredView('center'),
              HelperStyles.padding(16, 24),
            ]}>
            <FontAwesomeIcons
              color={Colors.slateGray}
              name={handleFileIcon(getFileExtension(fileData.filename))}
              size={24}
            />
          </View>

          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justView('justifyContent', 'space-between'),
              HelperStyles.padding(8, 8),
            ]}>
            <View
              style={[
                HelperStyles.flex(Boolean(fileData?.counts) ? 0.75 : 1),
                HelperStyles.justView('justifyContent', 'center'),
              ]}>
              <Text
                numberOfLines={1}
                style={HelperStyles.textView(
                  14,
                  '600',
                  Colors.lightText,
                  'left',
                  'none',
                )}>
                {fileData.filename}
              </Text>
            </View>

            {Boolean(fileData?.counts) && (
              <View
                style={[
                  HelperStyles.flex(0.25),
                  HelperStyles.justifyContentCenteredView('center'),
                ]}>
                <Text
                  style={[
                    HelperStyles.textView(
                      10,
                      'bold',
                      Colors.white,
                      'left',
                      'none',
                    ),
                    HelperStyles.padding(8, 2),
                    HelperStyles.justView('backgroundColor', Colors.tertiary),
                    HelperStyles.justView('borderRadius', 4),
                  ]}>
                  {fileData.counts}
                </Text>
              </View>
            )}
          </View>
        </Card>
      );
    };

    const handleFileIcon = fileExt => {
      switch (fileExt) {
        case 'css':
          return 'css';

        case 'html':
          return 'html5';

        case 'js':
          return 'js';

        default:
          return 'file-alt';
      }
    };

    return (
      <FlatList
        data={files}
        horizontal={false}
        numColumns={2}
        keyboardShouldPersistTaps={'handled'}
        keyExtractor={(_, index) => index}
        renderItem={({item, index}) => renderItem(item, index)}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  const renderFilesSkeleton = () => {
    return (
      <View
        style={[
          HelperStyles.flexDirection('row'),
          HelperStyles.justView('justifyContent', 'space-between'),
          HelperStyles.margin(0, 8),
        ]}>
        {renderSkeletonCard()}

        {renderSkeletonCard()}
      </View>
    );
  };

  const renderSkeletonCard = () => {
    return (
      <SkeletonPlaceholder>
        <View style={Styles.skeletonCardContainer} />
      </SkeletonPlaceholder>
    );
  };

  const renderReplaceModal = () => {
    return (
      <CustomModal
        showDefaultChildren={false}
        onNegative={() => {
          handleReplaceModal();
        }}
        onPositive={() => {
          handleReplaceModal();
        }}
        onRequestClose={() => {
          handleReplaceModal();
        }}
        visible={replaceModal}>
        <Text
          style={[
            HelperStyles.textView(16, '600', Colors.primary, 'center', 'none'),
            HelperStyles.justView('lineHeight', 16),
            HelperStyles.margin(0, 16),
          ]}>
          {Labels.replaceModal}
        </Text>

        <CustomTextInput
          onChangeText={txt => {
            setReplaceQuery(txt);
          }}
          onClose={() => {
            setReplaceQuery(null);
          }}
          placeholder={Labels.replace}
          value={replaceQuery}
        />

        <Button
          containerStyle={Styles.replaceButtonContainer}
          label={Labels.replace}
          onPress={() => {
            setLoading(true);

            setTotalWordCounts(null);

            setTimeout(() => {
              setConvertStatus(true);

              replaceWordInDirFiles();

              setLoading(false);
            }, 2500);

            handleReplaceModal();
          }}
        />
      </CustomModal>
    );
  };

  const handleReplaceModal = () => {
    setReplaceModal(!replaceModal);
  };

  const replaceWordInDirFiles = async () => {
    if (Boolean(replaceQuery)) {
      await files.map(fileData => {
        RNFetchBlob.fs
          .readStream(fileData.path, 'utf8', -1, 1000 / 200)
          .then(stream => {
            let content = '';

            stream.open();

            stream.onData(chunk => {
              content += chunk;
            });

            stream.onEnd(() => {
              content = content.split(searchQuery).join(replaceQuery);

              fileData.counts = null;

              selectedDirs.map(dirName => {
                if (Boolean(dirName)) {
                  createFilestoDir(dirName, content);
                } else {
                  showMessage({
                    description: `Sorry! Unable to update the file(s) from ${dirName} dir!`,
                    icon: 'auto',
                    message: Labels.error,
                    type: 'danger',
                  });
                }
              });
            });
          });

        return fileData;
      });

      showMessage({
        description: Labels.replaceSuccess,
        icon: 'auto',
        message: Labels.success,
        type: 'success',
      });
    } else {
      showMessage({
        description: Labels.sampleFileCache,
        icon: 'auto',
        message: Labels.error,
        type: 'danger',
      });
    }

    setSearchQuery(null);

    setReplaceQuery(null);

    setFiles(files);
  };

  const renderLogoutModal = () => {
    return (
      <CustomModal
        message={Labels.exitApp}
        onNegative={() => {
          handleExitApp();
        }}
        onPositive={() => {
          handleExitApp();

          BackHandler.exitApp();
        }}
        onRequestClose={() => {
          handleExitApp();
        }}
        visible={exitApp}
      />
    );
  };

  const handleExitApp = () => {
    setExitApp(!exitApp);
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={[HelperStyles.flex(1), HelperStyles.screenSubContainer]}>
        <View
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justView('justifyContent', 'space-between'),
          ]}>
          <View
            style={[
              HelperStyles.flex(
                Boolean(searchQuery) && Boolean(convertStatus)
                  ? 0.8
                  : Boolean(searchQuery) || Boolean(convertStatus)
                  ? 0.9
                  : 1,
              ),
              HelperStyles.justView('justifyContent', 'center'),
            ]}>
            <Text
              style={HelperStyles.textView(
                18,
                'bold',
                Theme.primary,
                'left',
                'none',
              )}>
              {Labels.files}
            </Text>
          </View>

          {Boolean(convertStatus) && (
            <View
              style={[
                HelperStyles.flex(0.1),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <TouchableOpacity
                onPress={() => {
                  handleDirFilesZipConvert();
                }}>
                <FontAwesomeIcons
                  color={Theme.text}
                  name={'file-archive'}
                  size={20}
                  style={HelperStyles.padding(2, 2)}
                />
              </TouchableOpacity>
            </View>
          )}

          {Boolean(searchQuery) && (
            <View
              style={[
                HelperStyles.flex(0.1),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <TouchableOpacity
                onPress={() => {
                  handleReplaceModal();
                }}>
                <FontAwesomeIcons
                  color={Theme.text}
                  name={'redo-alt'}
                  size={20}
                  style={HelperStyles.padding(2, 2)}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>

        {renderDirectory()}

        {renderSearchBar()}

        {Boolean(totalWordCounts) && renderWordCounts()}

        {!Boolean(loading) & Boolean(files) && Array.isArray(files) ? (
          files.length != 0 ? (
            renderFiles()
          ) : (
            <NoResponse />
          )
        ) : (
          <>
            {renderFilesSkeleton()}

            {renderFilesSkeleton()}

            {renderFilesSkeleton()}

            {renderFilesSkeleton()}

            {renderFilesSkeleton()}
          </>
        )}
      </View>

      {renderReplaceModal()}

      {renderLogoutModal()}
    </View>
  );
};

export default Dashboard;
