import React, {useCallback} from 'react';
import {Image, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Labels from '../../utils/Strings';
import Styles from '../../styles/authStyles/Splash';
import * as HelperStyles from '../../utils/HelperStyles';

const Splash = props => {
  // Splash Variables

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      init();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const init = async () => {
    setTimeout(() => {
      props.navigation.navigate(Labels.dashboard);
    }, 2500);
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Image
            resizeMode={'contain'}
            source={Assets.logo}
            style={HelperStyles.imageView(120, 120)}
          />
        </View>
      </View>
    </View>
  );
};

export default Splash;
