import React, {useEffect} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
  useTheme,
} from '@react-navigation/native';
import {Security} from './src/components/otherComponents/Security';
import Colors from './src/utils/Colors';
import FlashMessage from 'react-native-flash-message';
import Indicator from './src/components/appComponents/Indicator';
import Labels from './src/utils/Strings';
import RootNavigation from './src/navigations/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import * as Helpers from './src/utils/Helpers';
import * as HelperStyles from './src/utils/HelperStyles';

const App = () => {
  // Configuration Variables
  // Dark Theme
  const customDarkTheme = {
    ...DarkTheme,
    colors: {
      ...DarkTheme.colors,
      background: Colors.black,
      primary: Colors.primary,
      text: Colors.white,
    },
  };

  // Light Theme
  const customLightTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: Colors.white,
      primary: Colors.primary,
      text: Colors.black,
    },
  };

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  useEffect(() => {
    let isFocus = true;

    // For react-native-splash-screen configuration
    SplashScreen.hide();

    return () => {
      isFocus = false;
    };
  }, []);

  return (
    <SafeAreaView style={HelperStyles.screenContainer(Theme.background)}>
      <StatusBar barStyle={'light-content'} backgroundColor={Colors.black} />

      <NavigationContainer
        fallback={<Indicator />}
        theme={themeScheme == Labels.dark ? customDarkTheme : customLightTheme}>
        <RootNavigation />
      </NavigationContainer>

      {/* Custom Plugin Provider */}
      <FlashMessage duration={2000} floating={true} position={'top'} />
    </SafeAreaView>
  );
};

export default Security(App);
